﻿using KaasBot.Entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace KaasBot
{
	public class KaasBot
	{
		private const string ApiBase = "https://api.telegram.org";
		private const string LastUpdateKey = "LastUpdate";

		private HttpClient _client = new HttpClient();

		private object _syncRoot = new object();

		private TextDb _db;

		private bool _isRunning;

		private string _apiToken;

		private Dictionary<string,string> dict = new Dictionary<string,string>();

		public KaasBot(string apiToken)
		{
			_apiToken = apiToken;
			_db = new TextDb("kaasbot.txt");
			dict["kaas"] = "kaas!";	
			dict["cheese"] = "kaas!";	
			dict["tsiis"] = "kaas!";	
			dict["queso"] = "no hablo espanol";	
			dict["fromage"] = "kaas!";	
			dict["Käse"] = "kaas!";	
			dict["keshi"] = "kaas!";	
			dict["keju"] = "kaas!";	
		}

		public async void Start()
		{
			lock (_syncRoot)
			{
				if (_isRunning)
				{
					return;
				}

				_isRunning = true;
			}
			HttpPoller();
		}

		private async void HttpPoller(Task task = null)
		{
			Thread.Sleep(500);
			_client.GetAsync(BuildUpdateUri(_db.Get<int>(LastUpdateKey))).ContinueWith(ProcessResponse).ContinueWith(HttpPoller);
		}

		private async void ProcessResponse(Task<HttpResponseMessage> responseTask)
		{
			ApiResponse<List<Update>> response = await ParseResponse<List<Update>>(responseTask.Result);
			if (response.Success && response.Result.Count > 0)
			{
				foreach (Update update in response.Result)
				{
					ProcessUpdate(update);
				}

				_db.Set(LastUpdateKey, response.Result.Last().Id);
				Console.WriteLine("Received {0} updates. Last update ID: {1}", response.Result.Count, response.Result.Last().Id);
			}
		}

		private async void ProcessUpdate(Update update)
		{
			foreach (KeyValuePair<string, string> pair in dict)
			{
				if (update.Message.Text != null && update.Message.Text.StartsWith(pair.Key))
				{
					HttpResponseMessage response = await PostToUri(BuildMethodUri("sendMessage"), new OutboundTextMessage() { Chat = update.Message.Chat, Text = pair.Value });
				}
			}			
		}

		private string BuildUpdateUri(int lastUpdateId = 0)
		{
			return (lastUpdateId > 0)
				? String.Format("{0}?offset={1}", BuildMethodUri("getUpdates"), lastUpdateId + 1)
				: BuildMethodUri("getUpdates")
			;
		}

		private string BuildMethodUri(string method)
		{
			return String.Format("{0}/bot{1}/{2}", ApiBase, _apiToken, method);
		}

		private async Task<HttpResponseMessage> PostToUri(string uri, object data)
		{
			return await _client.PostAsync(uri, new FormUrlEncodedContent(JsonConvert.DeserializeObject<Dictionary<string, string>>(JsonConvert.SerializeObject(data))));
		}

		private async Task<ApiResponse<T>> ParseResponse<T>(HttpResponseMessage response)
		{
			response.EnsureSuccessStatusCode();
			using (StreamContent stream = response.Content as StreamContent)
			{
				string json = await stream.ReadAsStringAsync();
				return JsonConvert.DeserializeObject<ApiResponse<T>>(json);
			}
		}
	}
}