﻿using KaasBot.Helpers;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace KaasBot.Entities
{
	public class OutboundTextMessage
	{
		[JsonProperty("chat_id")]
		[JsonConverter(typeof(JsonIConversationIdConverter))]
		public IConversation Chat { get; set; }

		[JsonProperty("text")]
		public string Text { get; set; }

		[JsonProperty("disable_web_page_preview")]
		public bool DisableWebPagePreview { get; set; }
	}
}