﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace KaasBot.Entities
{
	public class Update
	{
		[JsonProperty("update_id")]
		public int Id { get; set; }

		[JsonProperty("message")]
		public Message Message { get; set; }
	}
}