﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;

namespace KaasBot.Entities
{
	public class ApiResponse<T>
	{
		[JsonProperty("ok")]
		public bool Success { get; set; }

		[JsonProperty("result")]
		public T Result { get; set; }
	}
}