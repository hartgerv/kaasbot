﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace KaasBot.Entities
{
	public class User : IConversation
	{
		[JsonProperty("id")]
		public int Id { get; set; }

		[JsonProperty("first_name")]
		public string FirstName { get; set; }

		[JsonProperty("last_name")]
		public string LastName { get; set; }

		[JsonProperty("username")]
		public string Username { get; set; }
	}
}