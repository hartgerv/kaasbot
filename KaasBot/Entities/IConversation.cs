﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace KaasBot.Entities
{
	public interface IConversation
	{
		int Id { get; set; }
	}
}