﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace KaasBot.Entities
{
	public class Groupchat : IConversation
	{
		[JsonProperty("id")]
		public int Id { get; set; }

		[JsonProperty("title")]
		public string Title { get; set; }
	}
}