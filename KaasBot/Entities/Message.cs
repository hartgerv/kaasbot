﻿using KaasBot.Helpers;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace KaasBot.Entities
{
	public class Message
	{
		[JsonProperty("message_id")]
		public int Id { get; set; }

		[JsonProperty("from")]
		public User From { get; set; }

		[JsonProperty("date")]
		[JsonConverter(typeof(JsonUnixTimestampConverter))]
		public DateTime Timestamp { get; set; }

		[JsonProperty("chat")]
		[JsonConverter(typeof(JsonConversationTypeConverter))]
		public IConversation Chat { get; set; }

		[JsonProperty("text")]
		public string Text { get; set; }
	}
}