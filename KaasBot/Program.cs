﻿using System;
using System.IO;
using System.Threading.Tasks;

namespace KaasBot
{
	internal class Program
	{
		private static void Main(string[] args)
		{
			KaasBot bot = new KaasBot(File.ReadAllText("token.txt"));
			Task.Factory.StartNew(bot.Start, TaskCreationOptions.LongRunning);
			while (true) ;
		}
	}
}