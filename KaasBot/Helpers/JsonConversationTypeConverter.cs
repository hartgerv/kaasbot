﻿using KaasBot.Entities;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Linq;

namespace KaasBot.Helpers
{
	public class JsonConversationTypeConverter : JsonConverter
	{
		public override bool CanConvert(Type objectType)
		{
			return typeof(IConversation).IsAssignableFrom(objectType);
		}

		public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
		{
			if (!typeof(IConversation).IsAssignableFrom(objectType))
			{
				throw new NotSupportedException();
			}

			JObject jObject = JObject.Load(reader);
			if (jObject.Properties().Any(p => p.Name == "username"))
			{
				return jObject.ToObject<User>();
			}
			else
			{
				return jObject.ToObject<Groupchat>();
			}
		}

		public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
		{
			serializer.Serialize(writer, value);
		}
	}
}