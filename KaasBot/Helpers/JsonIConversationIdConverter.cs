﻿using KaasBot.Entities;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Linq;

namespace KaasBot.Helpers
{
	public class JsonIConversationIdConverter : JsonConverter
	{
		public override bool CanConvert(Type objectType)
		{
			return typeof(IConversation).IsAssignableFrom(objectType);
		}

		public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
		{
			throw new NotSupportedException();
		}

		public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
		{
			serializer.Serialize(writer, ((IConversation)value).Id);
		}
	}
}